## Ansible

Настройка

* Загружаем PC или VPS с образа Gentoo
* Устанавливаем пароль

```
echo "root:password" | chpasswd
```

* Запускаем sshd

```
/etc/init.d/sshd start
```

* Меняем IP в файле ansible/hosts.yml
* Запускаем установку Gentoo

```
cd ansible
ansible-playbook -i hosts.yml playbooks/gentoo/install.yml
```

## Конфигурации по умолчанию

Конфигурации по умолчанию в каталоге defaults

## Всякая всячина

* gentoo - пересборка образа для загрузки через PXE
* PHP - тест проверки подключения MySQL
* Proxmox - Скрипт создания образа сетевой загрузки Proxmox VE версии 4
* Python - сборник скриптов
* Zabbix - шаблоны и скрипты сборки метрик

## Ссылки

* https://SpecialistOff.NET/
