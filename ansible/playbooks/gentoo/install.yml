---
- name: 'Сборка Gentoo'
  hosts:
    - linuxbuild

  tasks:
  - name: Очистка диска и разметка GPT
    shell: sgdisk --zap-all /dev/sda
    tags:
      - хранилище

  - name: Выравнивание
    shell: sgdisk -a=2 /dev/sda
    tags:
      - хранилище

  - name: 'Создание раздела BIOS'
    shell: sgdisk --new=1:0:+2M --typecode=1:EF02 /dev/sda
    tags:
      - хранилище

  - name: 'Создание первого раздела (/boot): 8300 Ext4'
    shell: sgdisk -n=2:0:+1G --typecode=2:8300 --change-name=2:"boot" /dev/sda
    tags:
      - хранилище

  - name: 'Создание второго раздела (swap): 8200 swap'
    shell: sgdisk -n=3:0:+4G --typecode=3:8200 --change-name=3:"swap" /dev/sda
    tags:
      - хранилище

  - name: 'Создание третьего раздела (/): 8300 XFS'
    shell: sgdisk --largest-new=4 --typecode=4:8300 --change-name=4:"xfs" /dev/sda
    tags:
      - хранилище

  - name: Проверяем
    shell: sgdisk -p /dev/sda
    tags:
      - хранилище

  - name: Форматируем раздел /boot
    shell: mkfs -t ext4 /dev/sda2
    tags:
      - хранилище

  - name: Форматируем раздел swap
    shell: mkswap /dev/sda3
    tags:
      - хранилище

  - name: Форматируем раздел /
    shell: mkfs -t xfs -f /dev/sda4
    tags:
      - хранилище

  - name: Монтируем /
    shell: mount /dev/sda4 /mnt/gentoo
    tags:
      - хранилище

  - name: Создаём каталог /boot
    file:
      path: "{{ item }}"
      state: directory
    with_items:
      - /mnt/gentoo/boot
      - /mnt/gentoo/tmp
    tags:
      - хранилище

  - name: Монтируем /boot
    shell: mount /dev/sda2 /mnt/gentoo/boot
    tags:
      - хранилище

  - name: Монтируем swap
    shell: swapon /dev/sda3
    tags:
      - хранилище

  - name: Получаем имя current-stage3-amd64
    shell: "curl {{ gentoo_mirror }}/releases/amd64/autobuilds/latest-stage3-amd64-nomultilib.txt | tail -n1"
    register: current_stage

  - name: Скачиваем current-stage3-amd64
    shell: "wget -O current-stage3-amd64.tar.xz {{ gentoo_mirror }}/releases/amd64/autobuilds/{{ current_stage['stdout'].split(' ')[0] }}"
    args:
      chdir: /mnt/gentoo/tmp

  - name: Распаковываем current-stage3-amd64
    shell: tar xvJpf tmp/current-stage3-amd64.tar.xz
    args:
      chdir: /mnt/gentoo

  - name: Скачиваем portage-latest.tar.xz
    shell: "wget -O portage-latest.tar.xz {{ gentoo_mirror }}/snapshots/portage-latest.tar.xz"
    args:
      chdir: /mnt/gentoo/tmp

  - name: Распаковываем portage-latest.tar.xz
    shell: tar xvJpf ../tmp/portage-latest.tar.xz
    args:
      chdir: /mnt/gentoo/usr

  - name: Монтируем proc
    shell: mount -t proc proc /mnt/gentoo/proc
    tags:
      - filesystem

  - name: Монтируем sys
    shell: mount --rbind /sys /mnt/gentoo/sys
    tags:
      - filesystem

  - name: Монтируем dev
    shell: mount --rbind /dev /mnt/gentoo/dev
    tags:
      - filesystem

  - name: Копируем resolv.conf
    shell: cp -L /etc/resolv.conf /mnt/gentoo/etc/

  - name: Создаём каталог /etc/portage/repos.conf
    file:
      path: "{{ item }}"
      state: directory
    with_items:
      - /mnt/gentoo/etc/portage/repos.conf

  - name: Копируем gentoo.conf
    copy:
      src: "files/repos.conf/gentoo.conf"
      dest: "/mnt/gentoo/etc/portage/repos.conf/gentoo.conf"

  - name: Синхронизация пакетов
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; emerge --sync'

  - name: Утилита app-portage/cpuid2cpuflags
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; emerge -v app-portage/cpuid2cpuflags'

  - name: Копирование конфигурационных файлов
    copy:
      src: "files/{{ item }}"
      dest: "/mnt/gentoo/etc/{{ item }}"
    with_items:
      - fstab
      - locale.gen
      - timezone
      - package.use
      - vconsole.conf

  - name: Копирование конфигурационных файлов
    copy:
      src: "files/{{ item }}"
      dest: "/mnt/gentoo/etc/portage/{{ item }}"
    with_items:
      - make.conf
      - package.use

  - name: Копирование конфигурационных файлов
    copy:
      src: files/package.use/world
      dest: /mnt/gentoo/etc/portage/package.use/world

  - name: Генерируем локали
    shell: locale-gen

  - name: Устанавливаем исходники ядра и утилиту сборки ядра
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; emerge -v gentoo-sources genkernel'

  - name: Собираем пакеты
    shell: "chroot /mnt/gentoo bash -c 'source /etc/profile; emerge -v {{ item }}'"
    with_items:
      - sys-fs/lvm2
      - sys-fs/mdadm
      - dev-db/sqlite
      - dev-libs/openssl
      - sys-libs/zlib
      - sys-fs/cryptsetup
      - sys-fs/e2fsprogs
      - sys-fs/btrfs-progs
      - sys-fs/xfsdump
      - sys-fs/xfsprogs
      # - sys-fs/zfs # Отключено из-за ошибки конфигурирования ядра
      - sys-apps/busybox
      - app-shells/bash
      - dev-lang/python:3.6

  - name: Собираем ядро
    # shell: chroot /mnt/gentoo bash -c 'source /etc/profile; genkernel --mdmadm --lvm --luks --nfs --btrfs --zfs --ssh --splash=emergence all'
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; genkernel --nfs --ssh --splash=emergence all'

  - name: Собираем пакеты
    shell: "chroot /mnt/gentoo bash -c 'source /etc/profile; emerge -v {{ item }}'"
    with_items:
      - syslog-ng
      - Locale-gettext
      - dhcpcd
      - pam
      - sys-process/cronie
      - pwgen
      - sys-boot/grub
      - sys-boot/os-prober
      - app-editors/nano
      - net-misc/openssh
      - app-misc/mc
      - net-dialup/ppp
      - net-wireless/iw
      - net-wireless/wpa_supplicant
      - media-fonts/liberation-fonts
      - net-firewall/ipset
      - net-firewall/iptables
      - dev-vcs/git
      - sys-apps/dbus

  - name: Установка GRUB на загрузочный диск
    shell: "rc-update add {{ item }} default"
    with_items:
      - dbus
      - sshd

  - name: Установка GRUB на загрузочный диск
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; grub-install /dev/sda'

  - name: Создание конфигурации GRUB
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; grub-mkconfig -o /boot/grub/grub.cfg'

  - name: Конвертация шрифта для GRUB
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; grub-mkfont --output=/boot/grub/fonts/LiberationMono-Regular.pf2 --size=24 /usr/share/fonts/liberation/LiberationMono-Regular.ttf'

  - name: Устанавливаем пароль для root
    shell: chroot /mnt/gentoo bash -c 'source /etc/profile; echo "root:root" | chpasswd'

  # - name: Скачиваем pip
  #   get_url:
  #     url: https://bootstrap.pypa.io/get-pip.py
  #     dest: /mnt/gentoo/root/get-pip.py

  # - name: Устанавливаем pip
  #   shell: chroot /mnt/gentoo bash -c 'source /etc/profile; python3.6 /root/get-pip.py'
  #   args:
  #     chdir: /mnt/gentoo/root

  # - name: Копирование requirements.txt
  #   copy:
  #     src: files/requirements.txt
  #     dest: /mnt/gentoo/root/requirements.txt

  # - name: Устанавливаем пакеты pip
  #   shell: chroot /mnt/gentoo bash -c 'source /etc/profile; pip3.6 install -r /root/requirements.txt'
  #   args:
  #     chdir: /mnt/gentoo/root
